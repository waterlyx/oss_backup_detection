<?php
/**
 * Created by PhpStorm.
 * User: liuyexing
 * Date: 2018/9/14
 * Time: 9:11
 */
$config = require('./config.php');
require('./bin/common.php');
$data = read($config['file_name']);

$time = '_'.$config['year'].$config['month'];

$get_data = array();
foreach ($data as $k => $v){
    if($k == 0){
        continue;
    }

    $time_position = strpos($v[0], $time);
    if($time_position === false){
        continue;
    }

    $get_name = substr($v[0], 0, strpos($v[0], $time));
    $get_name = str_replace('.', '_', $get_name);
    $get_name = str_replace('-', '_', $get_name);
    $get_time = substr($v[0], $time_position+1, 8);

    if(strpos($v[0], 'Web_') === 0){
        $get_data[substr($get_name,4)]['web'][] = $get_time;
    }
    if(strpos($v[0], 'Db_') === 0){
        $get_data[substr($get_name,3)]['db'][] = $get_time;
    }
}

$str = '';
foreach ($get_data as $kk => $vv){
    $str .= '############################'."\n";
    $str .= '网站名称：'.$kk."\n";
    if(!empty($vv['web'])){
        foreach ($vv['web'] as $vvv){
            $str .= 'web备份时间：'.$vvv."\n";
        }
    }else{
        $str .= 'web没有备份!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'."\n";
    }

    if(!empty($vv['db'])){
        foreach ($vv['db'] as $vvv){
            $str .= 'db备份时间：'.$vvv."\n";
        }
    }else{
        $str .= 'db没有备份!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'."\n";
    }

    $str .= '############################'."\n\n";
}

$str .= '检测完毕，共检测网站'.count($get_data).'个';

file_put_contents('./record/'.time().'.txt', $str);